﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace cra5000_webapi.Models
{
    interface IDocumentProvider
    {
        Task<List<XElement>> GetDocuments(string serialNumber);


    }
}
