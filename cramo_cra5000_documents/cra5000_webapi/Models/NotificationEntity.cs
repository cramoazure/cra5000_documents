﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace cra5000_webapi.Models
{
    public class NotificationEntity : TableEntity
    {

        public NotificationEntity()
        {

        }

        public NotificationEntity(string customerId, string uniquekey)
        {
            this.PartitionKey = customerId;
            this.RowKey = uniquekey;
        }

        public string Notification { get; set; }
        public string Type { get; set; }
        public string Order { get; set; }
        public string Market { get; set;}
        public string NotificationDateTime { get; set; }

    }
}