﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace cra5000_webapi.Models
{
    public class DekraDocuments : IDocumentProvider
    {

        public async Task<List<XElement>> GetDocuments(string serialNumber)
        {
            using (var client = new HttpClient())
            {

                string baseAddress = System.Configuration.ConfigurationManager.AppSettings["DocumentsProxy.BaseAddress"];
                string uri = System.Configuration.ConfigurationManager.AppSettings["DocumentsProxy.Uri"] + "DEKRA?serial=" + serialNumber;
                //string subscriptionKey = System.Configuration.ConfigurationManager.AppSettings["360.SubscriptionKey"];

                //string baseAddress = "https://se.bs.cramo.com";
                //string uri = "/pim-web/pim/resources/DEKRA?serial=" + serialNumber;

                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(uri);

                List<XElement> entityList = new List<XElement>();

                if (response.IsSuccessStatusCode)
                {
                    List<Document> dList = JsonConvert.DeserializeObject<List<Document>>(await response.Content.ReadAsStringAsync());

                    foreach (Document d in dList)
                    {
                        XElement docEntity = new XElement("DocumentEntity");

                        docEntity.Add(new XElement("Source", "Dekra"));
                        docEntity.Add(new XElement("Type", d.type));
                        docEntity.Add(new XElement("Name", d.name));
                        docEntity.Add(new XElement("Url", d.url));

                        entityList.Add(docEntity);
                    }
                }

                return entityList;

            }


        }
    }
}