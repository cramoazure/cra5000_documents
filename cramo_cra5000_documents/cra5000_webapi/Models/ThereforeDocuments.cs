﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace cra5000_webapi.Models
{
    public class ThereforeDocuments : IDocumentProvider
    {
        public async Task<List<XElement>> GetDocuments(string serialNumber)
        {
            using (var client = new HttpClient())
            {

                string baseAddress = System.Configuration.ConfigurationManager.AppSettings["DocumentsProxy.BaseAddress"];
                string uri = System.Configuration.ConfigurationManager.AppSettings["DocumentsProxy.Uri"] + "THEREFORE?serial=" + serialNumber;

                string thereforeBaseAddress = System.Configuration.ConfigurationManager.AppSettings["Therefore.BaseAddress"];

                //string baseAddress = "https://se.bs.cramo.com";
                //string uri = "/pim-web/pim/resources/THEREFORE?serial=" + serialNumber;

                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(uri);

                List<XElement> entityList = new List<XElement>();

                if (response.IsSuccessStatusCode)
                {

                    List<Document> dList = JsonConvert.DeserializeObject<List<Document>>(await response.Content.ReadAsStringAsync());

                    foreach (Document d in dList)
                    {
                        XElement docEntity = new XElement("DocumentEntity");

                        docEntity.Add(new XElement("Source", "Therefore"));
                        docEntity.Add(new XElement("Type", (d.type == "null" ? string.Empty : d.type)));
                        docEntity.Add(new XElement("Name", d.name));
                        docEntity.Add(new XElement("Url", thereforeBaseAddress + d.url));
                        entityList.Add(docEntity);
                    }
                }

                return entityList;

            }


        }
    }
}