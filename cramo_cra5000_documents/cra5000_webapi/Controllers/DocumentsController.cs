﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cramo.Utils.Authentication.Filters;
using Cramo.Utils.Logging;
using cra5000_webapi.Models;
using System.Xml.Linq;
using System.Threading.Tasks;
using Cramo.Utils.Json;

namespace cra5000_webapi.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class DocumentsController : ApiController
    {
        private Logger _logger;

        [HttpGet]
        public async Task<HttpResponseMessage> GET(string serialNumber, string marketId)
        { 
        
            // Log that GET request has been received
            _logger = new Logger(this.GetType().Name);
            string logkey = Guid.NewGuid().ToString();
            _logger.Log(Loglevel.Trace, logkey, "10", "Get request received", string.Format("Serial number: {0}, marketId: {1}", serialNumber, marketId));
            
            var providerCollection = new List<IDocumentProvider>();

            providerCollection.Add(new DekraDocuments());
            providerCollection.Add(new ThereforeDocuments());

            XElement docList = new XElement("DocumentList");          

            foreach (IDocumentProvider docProvider in providerCollection)
            {
                List<XElement> docEntities = await docProvider.GetDocuments(serialNumber);
                
                foreach (XElement entity in docEntities)
                {
                    docList.Add(entity);
                }
            }

            XDocument xdoc = new XDocument(
                new XElement("Documents",
                    new XElement("MarketId", marketId),
                    new XElement("SerialNo", serialNumber),
                    docList));

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

            string responseString;

            if (Request.Headers.Accept.ToString() == "application/json")
            {
                responseString = JsonConverter.GetJsonString(xdoc.ToString(), "cra5000_webapi.Resources.Documents.xsd");
            }
            else
            {
                responseString = xdoc.ToString();
            }

            response.Content = new StringContent(responseString);
            _logger.Log(Loglevel.Trace, logkey, "20", "Message delivered", responseString);

            return response;

        
        }

    }
}
