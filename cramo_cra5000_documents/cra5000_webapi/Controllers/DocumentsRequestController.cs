﻿using System;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using Microsoft.ServiceBus.Messaging;
using cra5000_webapi.Models;
using cra5000_webapi.Filters;

namespace cra5000_webapi.Controllers
{
    [IdentityBasicAuthentication]
    [Authorize]
    public class DocumentsRequestController : ApiController
    {
        
        [HttpGet]
        public HttpResponseMessage Get(string productId)
        {
            HttpResponseMessage response = new HttpResponseMessage();

            try
            {
                // get connection string from web.config
                string connectionString = System.Configuration.ConfigurationManager.AppSettings["Microsoft.ServiceBus.ConnectionString"];
                    
                // send message to queue

                QueueClient client = QueueClient.CreateFromConnectionString(connectionString, "DocumentRequests");

                client.Send(new BrokeredMessage(new DocumentRequest() {productId = productId}));

                // return ok
                response.Content = new StringContent("Request for product " + productId + " has been received successfully.");
                response.StatusCode = HttpStatusCode.OK;

            }
            catch (Exception e)
            {
                // all exceptions handled as 500's
                response.Content = new StringContent(e.ToString());
                response.StatusCode = HttpStatusCode.InternalServerError;
            }
            
            
            return response;
            
            
            
            
        }


    }
}