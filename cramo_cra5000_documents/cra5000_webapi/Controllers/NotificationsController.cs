﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Cramo.Utils.Authentication;
using Cramo.Utils.Authentication.Filters;
using Cramo.Utils.Logging;
using System.Xml.Linq;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.Azure;
using cra5000_webapi.Models;
using Cramo.Utils.Json;

namespace cra5000_webapi.Controllers
{
    [Authorize]
    [IdentityBasicAuthentication]
    public class NotificationsController : ApiController
    {
        private Logger _logger;
        private XNamespace _notificationsNamespace = "http://www.cramo.com/canonical";
        private int MAX_BATCH_SIZE = 100;

        [HttpPost]
        public HttpResponseMessage POST([FromBody]string notifications)
        {

            _logger = new Logger(this.GetType().Name);

            _logger.Log(Loglevel.Trace, string.Empty, "10", "POST request to NotificationsController received.", notifications);

            // Get market from message
            XDocument xdoc = XDocument.Load(new StringReader(notifications));

            XElement market = (from xml in xdoc.Root.Descendants(_notificationsNamespace + "ReferenceID")
                               select xml).FirstOrDefault();

            XElement transId = (from xml in xdoc.Root.Descendants(_notificationsNamespace + "BODID")
                                select xml).FirstOrDefault();



            // Init table storage
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("Storage.ConnectionString"));
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable notificationsTable = tableClient.GetTableReference("notifications");
            notificationsTable.CreateIfNotExists();

            // prepare insert operations

            TableBatchOperation batchInsertOperation = new TableBatchOperation();

            IEnumerable<XElement> listOfLocations = from loc in xdoc.Root.Descendants(_notificationsNamespace + "Notification")
                                                    select loc;

            foreach (XElement xloc in listOfLocations)
            {

                var customerId = xloc.Element(_notificationsNamespace + "BusinessPartnerId").Value;
                var orderNumber = xloc.Element(_notificationsNamespace + "Order").Value;
                var notificationType = xloc.Element(_notificationsNamespace + "NotificationType").Value;
                var notificationDateTime = xloc.Element(_notificationsNamespace + "NotificationDateTime").Value;

                // Prepare entity
                NotificationEntity notification = new NotificationEntity(customerId, Guid.NewGuid().ToString());
                notification.Notification = xloc.ToString();
                notification.Order = orderNumber;
                notification.Type = notificationType;
                notification.Market = market.Value;
                notification.NotificationDateTime = notificationDateTime;

                batchInsertOperation.Insert(notification);

                // commit if batch is full
                if (batchInsertOperation.Count == MAX_BATCH_SIZE)
                {
                    notificationsTable.ExecuteBatch(batchInsertOperation);
                    batchInsertOperation = new TableBatchOperation();
                }

            }

            // commit last batch
            if (batchInsertOperation.Count > 0)
            {
                notificationsTable.ExecuteBatch(batchInsertOperation);
            }

            _logger.Log(Loglevel.Trace, transId.Value, "20", "Notification saved to storage", string.Empty);

            return new HttpResponseMessage(HttpStatusCode.OK);

        }

        [HttpGet]
        public HttpResponseMessage GET(string marketId, string customerId, string type = "all", string orderNumber = "")
        {
            string logkey = Guid.NewGuid().ToString();

            _logger = new Logger(this.GetType().Name);
            _logger.Log(Loglevel.Trace, logkey, "10", "GET request received", string.Format("MarketId: {0}, customerId: {1}, type: {2}, ordernumber: {3}", marketId, customerId, type, orderNumber));

            NotificationType _notificationtype;

            if (!Enum.TryParse(type, out _notificationtype) || !Enum.IsDefined(typeof(NotificationType), _notificationtype))
            {

                _logger.Log(Loglevel.Error, logkey, "900", "Unknown notification type: " + type, string.Empty);

                // HTTP 400 bad request

                HttpResponseMessage badrequest = new HttpResponseMessage(HttpStatusCode.BadRequest);
                badrequest.Content = new StringContent("Unknown notification type.");

                return badrequest;

            }

            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("Storage.ConnectionString"));
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable notificationsTable = tableClient.GetTableReference("notifications");

            TableQuery<NotificationEntity> query = new TableQuery<NotificationEntity>().Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, customerId));

            XElement dataArea = new XElement(_notificationsNamespace + "DataArea");

            // select all notifications for specific customer, matching given marketId
            var notifications = from el in notificationsTable.ExecuteQuery(query).Where(o => o.Market.ToUpper() == marketId.ToUpper())
                                select el;

            // if type is specified, filter on that type
            if (_notificationtype != NotificationType.all)
            {
                notifications = notifications.Where(o => o.Type.ToLower() == _notificationtype.ToString().ToLower());
            }

            // only filter on ordernumber if provided
            if (orderNumber != string.Empty)
            {
                notifications = notifications.Where(o => o.Order == orderNumber);
            }

            TableBatchOperation batchDeleteOperation = new TableBatchOperation();

            foreach (var e in notifications)
            {

                dataArea.Add(XElement.Parse(e.Notification));

                batchDeleteOperation.Delete(e);

                // commit batch if full
                if (batchDeleteOperation.Count == MAX_BATCH_SIZE)
                {
                    notificationsTable.ExecuteBatch(batchDeleteOperation);
                    batchDeleteOperation = new TableBatchOperation();
                }

            }

            // commit last batch
            if (batchDeleteOperation.Count > 0)
            {
                notificationsTable.ExecuteBatch(batchDeleteOperation);
            }

            XDocument xdoc = new XDocument(
               new XElement(_notificationsNamespace + "Notifications",
                   new XAttribute("releaseID", "10"),
                   new XAttribute("versionID", "1"),
                   new XAttribute("systemEnvironmentCode", "TEST"),
                   new XAttribute("languageCode", "sv"),
                   new XElement(_notificationsNamespace + "ApplicationArea",
                       new XElement(_notificationsNamespace + "Sender",
                           new XElement(_notificationsNamespace + "ReferenceID", marketId)),
                           new XElement(_notificationsNamespace + "CreationDateTime", DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")),
                           new XElement(_notificationsNamespace + "BODID", logkey)),
                           dataArea));


            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

            string responseString;

            if (Request.Headers.Accept.ToString() == "application/json")
            {
                 responseString = JsonConverter.GetJsonString(xdoc.ToString(), "cra5000_webapi.Resources.Notifications.xsd");
            }
            else
            {
                responseString = xdoc.ToString();
            }

            response.Content = new StringContent(responseString);

            _logger.Log(Loglevel.Trace, logkey, "20", "Message delivered.", responseString);

            return response;


        }


    }
}
