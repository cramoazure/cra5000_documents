﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Azure.WebJobs.ServiceBus;
using Microsoft.ServiceBus;

namespace cra5000_webjob
{
    // To learn more about Microsoft Azure WebJobs SDK, please see http://go.microsoft.com/fwlink/?LinkID=320976
    class Program
    {

        private static NamespaceManager _namespaceManager;

        // Please set the following connection strings in app.config for this WebJob to run:
        // AzureWebJobsDashboard and AzureWebJobsStorage
        static void Main()
        {

            string serviceBusConnectionString = AmbientConnectionStringProvider.Instance.GetConnectionString(ConnectionStringNames.ServiceBus);
            _namespaceManager = NamespaceManager.CreateFromConnectionString(serviceBusConnectionString);

            // check if queue exists

            if (!_namespaceManager.QueueExists(Functions.QueueName))
            {
                _namespaceManager.CreateQueue(Functions.QueueName);
            }

            JobHostConfiguration config = new JobHostConfiguration();

            ServiceBusConfiguration sb_config = new ServiceBusConfiguration
            {
                ConnectionString = serviceBusConnectionString
            };

            config.UseServiceBus(sb_config);

            JobHost host = new JobHost(config);
            
            host.RunAndBlock();
        }
    }
}
