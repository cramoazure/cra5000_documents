﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:cramo="www.cramo.com/integrations" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" exclude-result-prefixes="cramo" >
  <xsl:output indent="yes" cdata-section-elements="Psi PsiX Manual Image" />
  <xsl:template match="/">
    <DocumentLinkOrderResponse>
      <xsl:for-each select="/cramo:SRARoot/cramo:resources">
        <xsl:choose>
          <xsl:when test="./cramo:type = 'psi'">
            <Psi>
              <xsl:value-of select="cramo:url"/>
            </Psi>
          </xsl:when>
          <xsl:when test="./cramo:type = 'psix' or ./cramo:type = 'psi-x'">
            <PsiX>
              <xsl:value-of select="cramo:url"/>
            </PsiX>
          </xsl:when>
          <xsl:when test="./cramo:type = 'manual'">
            <Manual>
              <xsl:value-of select="cramo:url"/>
            </Manual>
          </xsl:when>
          <xsl:when test="./cramo:type = 'image'">
            <Image>
              <xsl:value-of select="cramo:url"/>
            </Image>
          </xsl:when>
          <xsl:otherwise>
            <!-- Otherwise nothing -->
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </DocumentLinkOrderResponse>
  </xsl:template>
</xsl:stylesheet>
