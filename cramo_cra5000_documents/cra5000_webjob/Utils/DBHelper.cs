﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace cra5000_webjob.Utils
{
    class DBHelper
    {
        public static void log(String applicationName, String requestKey, String stepId, String logText, String logMessage = "")
        {

            var _connectionStringEntry = ConfigurationManager.ConnectionStrings["cramo-esb-db"];

            string _connectionString = _connectionStringEntry.ConnectionString;

            using (SqlConnection _conn = new SqlConnection(_connectionString))
            {

                // 1.  create a command object identifying the stored procedure
                SqlCommand _cmd = new SqlCommand("sp_InsertLogRecord", _conn);

                // 2. set the command object so it knows to execute a stored procedure
                _cmd.CommandType = CommandType.StoredProcedure;

                // 3. add parameter to command, which will be passed to the stored procedure
                _cmd.Parameters.Add(new SqlParameter("@LogApplication", applicationName));
                _cmd.Parameters.Add(new SqlParameter("@LogKey", requestKey));
                _cmd.Parameters.Add(new SqlParameter("@LogStep", stepId));
                _cmd.Parameters.Add(new SqlParameter("@LogText", logText));
                _cmd.Parameters.Add(new SqlParameter("@LogMessage", logMessage));

                // execute the command
                _conn.Open();

                try
                {
                    _cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    _conn.Close();
                }

                
            }
        }

    }
}
