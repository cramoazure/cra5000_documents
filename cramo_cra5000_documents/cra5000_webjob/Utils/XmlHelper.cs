﻿using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Xsl;

namespace cra5000_webjob.Utils
{
    class XmlHelper
    {

        internal static string XslTransform(string stylesheetName, string inputXml)
        {

            XslCompiledTransform xct = new XslCompiledTransform();

            StringBuilder sb = new StringBuilder();
            XmlWriterSettings xws = new XmlWriterSettings();
            xws.Encoding = Encoding.UTF8;
            xws.Indent = true;
            xws.OmitXmlDeclaration = true;

            Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("cra5000_webjob.resources." + stylesheetName);
            xct.Load(XmlReader.Create(s));
            
            xct.Transform(XmlReader.Create(new StringReader(inputXml)), XmlWriter.Create(sb, xws));

            return sb.ToString();
            
        }


    }
}
