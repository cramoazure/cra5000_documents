﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.ServiceBus.Messaging;
using System.Net.Http;
using System.Net.Http.Headers;
using cra5000_webapi.Models;
using Newtonsoft.Json;
using System.Xml;
using cra5000_webjob.Utils;
using System.Net;

namespace cra5000_webjob
{


    public class Functions
    {

        public const string QueueName = "DocumentRequests";
        private const string LOG_APP_NAME = "SRADocuments";


        // This function will get triggered/executed when a new message is written 
        // on an Azure ServiceBus Queue called DocumentRequests_test.
        public static async void ProcessQueueMessage([ServiceBusTrigger(QueueName)] BrokeredMessage message, TextWriter log)
        {

            string _logkey = "DocumentsRequest_" + message.MessageId;

            DocumentRequest dr = message.GetBody<DocumentRequest>();

            DBHelper.log(LOG_APP_NAME, _logkey, "10", "Received DocumentsRequest for product " + dr.productId + ".");

            string response = await CallBackendAsync(dr.productId);

            string xslResponse = XmlHelper.XslTransform("SRARoot_To_DocumentLinkOrderResponse.xslt", response);

            await ReturnResponseToPIM(xslResponse, dr.productId, _logkey);

        }

        private static async Task<string> CallBackendAsync(string productId)
        {
            using (var client = new HttpClient())
            {

                string baseAddress = System.Configuration.ConfigurationManager.AppSettings["SRA.BaseAddress"];
                string uri = System.Configuration.ConfigurationManager.AppSettings["SRA.Uri"];

                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync(uri + productId);

                response.EnsureSuccessStatusCode();

                string json_response = await response.Content.ReadAsStringAsync();

                XmlDocument xdoc = JsonConvert.DeserializeXmlNode(json_response, "SRARoot");
                xdoc.DocumentElement.SetAttribute("xmlns", "www.cramo.com/integrations");

                return xdoc.OuterXml;

            }
        }

        private static async Task ReturnResponseToPIM(string documents, string productId, string _logkey)
        {
            using (var client = new HttpClient())
            {

                string baseAddress = System.Configuration.ConfigurationManager.AppSettings["PIM.BaseAddress"];
                string uri = System.Configuration.ConfigurationManager.AppSettings["PIM.Uri"];
                string subscriptionkey = System.Configuration.ConfigurationManager.AppSettings["PIM.SubscriptionKey"];

                client.BaseAddress = new Uri(baseAddress);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));

                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionkey);

                Console.Out.WriteLine("Sending request to " + uri + productId);

                HttpResponseMessage response = await client.PostAsync(uri + productId, new StringContent(documents));

                HttpStatusCode sc = response.StatusCode;

                string status = Convert.ToString((int)sc) + " " + sc.ToString();
                
                try
                {
                
                    EnsureSuccessStatusCode(response);

                    DBHelper.log(LOG_APP_NAME, _logkey, "20", "PIM status: " + status, await response.Content.ReadAsStringAsync());

                }
                catch (HttpRequestException ex)
                {

                    DBHelper.log(LOG_APP_NAME, _logkey, "990", "Failed while sending request to PIM: " + status, ex.ToString());
                
                }

            }
        }
        private static void EnsureSuccessStatusCode(HttpResponseMessage _responsemessage)
        {
            if (_responsemessage.StatusCode != HttpStatusCode.InternalServerError)
                _responsemessage.EnsureSuccessStatusCode();
        }
    }
}
